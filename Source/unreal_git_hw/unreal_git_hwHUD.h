// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "unreal_git_hwHUD.generated.h"

UCLASS()
class Aunreal_git_hwHUD : public AHUD
{
	GENERATED_BODY()

public:
	Aunreal_git_hwHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

