// Copyright Epic Games, Inc. All Rights Reserved.

#include "unreal_git_hwGameMode.h"
#include "unreal_git_hwHUD.h"
#include "unreal_git_hwCharacter.h"
#include "UObject/ConstructorHelpers.h"

Aunreal_git_hwGameMode::Aunreal_git_hwGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aunreal_git_hwHUD::StaticClass();
}
