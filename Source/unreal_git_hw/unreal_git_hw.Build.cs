// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class unreal_git_hw : ModuleRules
{
	public unreal_git_hw(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
