// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "unreal_git_hwGameMode.generated.h"

UCLASS(minimalapi)
class Aunreal_git_hwGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aunreal_git_hwGameMode();
};



